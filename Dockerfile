FROM alpine:3.9
MAINTAINER tobias@basinbah.de

ENV HUGO_VERSION 0.73.0
ENV HUGO_BINARY hugo
ENV HUGO_RESOURCE hugo_${HUGO_VERSION}_Linux-64bit
ENV TEMP_DIR /tmp/hugo/

RUN apk add --update \
    git \
    python \
    py-pip \
    ca-certificates \
    curl \
  && pip install pygments \
  && rm -rf /var/cache/apk/*

RUN update-ca-certificates || true
RUN curl -L -O https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_RESOURCE}.tar.gz

RUN mkdir ${TEMP_DIR} && \
    tar -xvf ./${HUGO_RESOURCE}.tar.gz -C ${TEMP_DIR} && \
    mv ${TEMP_DIR}${HUGO_BINARY} /usr/bin/hugo && \
    rm -rf ${TEMP_DIR}
